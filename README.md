# t6963-mcp23017 raspberry


Interface a t6963 (ye ol'e TLX-711 again) to a rapberry pi 1B, using
a mcp23017 to save GPIO pins.

I've set the i2c frequency to 1.55MHz, the mcp refuses to work @1.7 for some reason.


'schematic' :-)


![connection](https://raw.githubusercontent.com/rasp/t6963-mcp23017/master/23017.png)


Add a 100n C between Vss and Vdd !
