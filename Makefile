CC=gcc
CFLAGS=-I. -D MAKEFILE
LDFLAGS= -lpthread 
DEPS = gpio.h headers.h lcd.h font.h i2c.h Makefile
OBJ = gpio.o lcd.o font.o ds18.o main.o i2c.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

t6963: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) $(LDFLAGS)


