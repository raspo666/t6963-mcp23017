/*
 * Copyright (C) 2017 Ralph Spitzner
 *
  * this file id part of https://github.com/rasp/t6963-mcp23017
*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */


#include "headers.h"


unsigned char iob_val,ov,heat;





void lcd_rst()
    {
    unsigned char port;

    port = iob_val & ~RST;  // could operate on iob_val directly

    if(i2cw8(i2cfd,GPIOB,port) < 0)
        {
        perror("i2cw8 lcd_rst");
        exit(1);
        }

    usleep(1000);

    port |= RST;
    if(i2cw8(i2cfd,GPIOB,port) < 0)
        {
        perror("i2cw8 lcd_rst");
        exit(1);
        }


    usleep(1000);



    }


void lcd_data(unsigned char data)
    {
    unsigned short ports;

//    lcd_wait_status();

    ports = (iob_val & ~(WR | _CD)) << 8;
    ports |= data;
    if(i2cw16(i2cfd,GPIOA,ports) < 0)
        {
        perror("i2cw16 in lcd_data");
        exit(1);
        }
    SET_L(CS);
    delay();
    SET_H(CS);

    }




void lcd_cmd(unsigned char data)
    {

    unsigned short ports;
    ports = (iob_val & ~WR) << 8;
    ports |= data;

    if(i2cw16(i2cfd,GPIOA,ports) < 0)
        {
        perror("i2w16 lcd_cmd");
        exit(1);
        }

    SET_L(CS);
// let it settle
    delay();

    SET_H(CS);




    }


void lcd_wait_status()
    {
// no need to check status i2c is slow enough
    }

/*
void lcd_print_status()
    {
    char stat;

    stat = lcd_read_status();
    printf("byte = %x\n",stat);
    printf("STA0 = %s\n", stat & 1 ? "ready" : "inexec" );
    printf("STA1 = %s\n", stat & 2 ? "ready" : "rw occ");
    printf("STA2 = %s\n", stat & 4 ? "ready" : "autoread");
    printf("STA3 = %s\n", stat & 8 ? "ready" : "autowrite");
    printf("STA4 = %s\n", stat & 0x10 ? "nop" : "nop");
    printf("STA5 = %s\n", stat & 0x20 ? "op ok" : "op ?/" );
    printf("STA6 = %s\n", stat & 0x40 ? "peek/copy" : "no err" );
    printf("STA7 = %s\n", stat & 0x80 ? "blink on" : "blink off");

    }

*/

void lcd_init()
    {
    i2cfd = i2c_open(I2CDEV);
    if(i2cfd < 0)
        {
        perror("i2c_open in lcd_init");
        exit(1);
        }

    enslave(MCP,i2cfd);



    if(i2cw16(i2cfd,IODIRA,0x0) < 0)
        {
        perror("i2c set dir");
        exit(1);
        }

    if(i2cw16(i2cfd,IODIRB,OVSW) < 0) // set switch to input
        {
        perror("i2c set dir");
        exit(1);
        }






    if(i2cw8(i2cfd,IOCON,0) < 0) // should be there after RST anyway
        {
        perror("i2cw8 lcd_rst");
        exit(1);
        }

    iob_val = RST | WR | RD | _CD;



    /*  for(i =0; i != 22; i++)
            printf("register %02x = %02x\n",i,i2cr8(i2cfd,i));
        lcd_rst();
        printf("rst\n");
    */

    /* From application note */

    lcd_cmd(0x80); // OR,CG-ROM

    lcd_data(0);
    lcd_data(0);
    lcd_cmd(0x42); // set graphics home


    lcd_data(0x1e);
    lcd_data(0x00);
    lcd_cmd(0x43); // graphics area set

    lcd_data(0x0);
    lcd_data(0x0);
    lcd_cmd(0x40); // text home

    lcd_data(0x1e);
    lcd_data(0x00);
    lcd_cmd(0x41); // text area set

    lcd_data(0x0);
    lcd_data(0x0);
    lcd_cmd(0x24); // text adddress pointer


    lcd_cmd(0x97); //txt/gr on

    lcd_cmd(0xa7); // cursor 7 lines

    lcd_data(0x0);
    lcd_data(0x0);
    lcd_cmd(0x21); // set cursor


    }



void set_addr(int x, int y)
    {

    int addr;


    addr = (y * 30) + (x / 8) ;
    lcd_data(addr & 0xff);
    lcd_data((addr >> 8) & 0xff);
    lcd_cmd(0x24); //set address pointer

    }

void set_gr()
    {

    lcd_data(0x0);
    lcd_data(0x0);
    lcd_cmd(0x42); // graphics start

    lcd_data(0x1e); // 30 bytes/line
    lcd_data(0x00);
    lcd_cmd(0x43); // graphics area set

    lcd_cmd(0x80); //OR mode

    lcd_cmd(0x98); // gr on no txt/curs

    set_addr(0,0);
    }

void clr()
    {
    int i;

    for (i=0; i< (40*8) ; i++)      //clear screen in text memory
        {
        lcd_data(0x00);
        lcd_cmd(0xC0);
        }

    lcd_data(0x0);
    lcd_data(0x0);
    lcd_cmd(0x24); // text adddress pointer

    }


void write_ch(char c)
    {
    lcd_data(c-0x20);  // 'translate' ascii
    lcd_cmd(0xc0);
    }

void set_cur(int x,int y)
    {
    lcd_data(x);
    lcd_data(y);
    lcd_cmd(0x21);
    }


void fake_tty(char *txt)
    {
    char *p;
    int i,x,y;

    i = 0;

    p = txt;

    while(*p != '\0')
        {
        i++;
        y = i / 40;
        x = i % 40;
        write_ch(*p);
        set_cur(x,y);
        p++;
        usleep(300000);
        }



    }

IMG *load_bmp(char *name)
    {
    FILE *mapf;
    BMPHEAD head;
    unsigned char *bitmapImage;
    BMPINFO info;
    IMG *img;




    if(!(mapf = fopen(name,"rb")))
        {
        perror("read_bmp");
        printf("while trying to open %s\n",name);
        return NULL;
        }

    /*
    sizeof(BMPHEAD) gives me 16, not 14,
    reading 16 bytes into that struct produces bs....
    */

    fread(&head.bfType,1, 2,mapf);
    fread(&head.bfSize,1, 4,mapf);
    fread(&head.bfReserved1,1, 2,mapf);
    fread(&head.bfReserved2,1, 2,mapf);
    fread(&head.bOffBits,1, 4,mapf);



    if (head.bfType != 0x4D42)
        {
        fclose(mapf);
        printf("%s not a BMP file\n",name);
        return NULL;
        }


    if(!(fread(&info, 1,sizeof(BMPINFO),mapf)))
        {
        printf("read info failed on %s\n",name);
        return NULL;
        }


    fseek(mapf, head.bOffBits, SEEK_SET);
    bitmapImage = (unsigned char*)malloc(info.biSizeImage);
    if(bitmapImage == NULL)
        {
        fclose(mapf);
        printf("fail to allocate %ld bytes !\n",info.biSizeImage);
        return NULL;
        }

    if(fread(bitmapImage,1,info.biSizeImage,mapf) != info.biSizeImage)
        {
        printf("read data %ld failed on %s\n",info.biSizeImage,name);
        fclose(mapf);
        return NULL;
        }
    fclose(mapf);

    if(info.biCompression != 0)
        {
        printf("won't handle compression\n");
        free(bitmapImage);
        return NULL;
        }
    if(info.biHeight > 64 || info.biWidth > 240)
        {
        printf("will not display w%ld x h%ld pixels on this display\n",
               info.biWidth,info.biHeight);
        free(bitmapImage);
        return NULL;
        }
    if(info.biBitCount > 1)
        {
        printf("this is a bicolor display !\n");
        free(bitmapImage);
        return NULL;
        }

    if(!(img = malloc(sizeof(IMG))))
        {
        printf("no mem for IMG struct ?\n");
        free(bitmapImage);
        return NULL;
        }
    img->width = info.biWidth & 0xff;
    img->height = info.biHeight & 0xff;
    img->data = bitmapImage;
//printf("img data size %d\n",info.biSizeImage);
//printf("img w %d h %d\n",info.biWidth,info.biHeight);

    return img;
    }



void clr_gr()
    {

    memset(pf,0x0,1920);
    set_addr(0,0);
    fastg(pf);

    }

/*
unsigned char getpixel(unsigned char x, unsigned char y)
    {
    unsigned char pixel;

    set_addr(x,y);
    lcd_cmd(0xc1);
    pixel = lcd_read_data() & 1<<(x%8); // WERK is this right ?
    return pixel;
    }
*/

/* swap bits in a byte from stackover - not used */
unsigned char swap_b(unsigned char b)
    {
    b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
    b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
    b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
    return b;

    }



void setpixel(unsigned char x, unsigned char y,unsigned char pixel)
    {
    unsigned char b;

    set_addr(x,y);

    b = 7 - (x%8);
    b |= pixel ? 0xf8 : 0xf0;
    lcd_cmd(b);
    }

void setpixel_m(unsigned char x, unsigned char y,unsigned char pixel)
    {
    unsigned char b;
    char *p;


    p = pf + (y * 30) + (x / 8);
    b = 7 - (x%8);
    if(pixel)
        *p |= 1<<b;
    else
        *p &= ~(1<<b);

    }




void show_image(IMG *img,int x,int y)
    {
    int px,py;
    char *p,bi,padBytes;

    p = (char *)img->data;

    if (((img->width % 32) == 0) || ((img->width % 32) > 24))
        padBytes = 0;
    else if ((img->width % 32) <= 8)
        padBytes = 3;
    else if ((img->width % 32) <= 16)
        padBytes = 2;
    else
        padBytes = 1;


    for(py = img->height-1; py > -1; py--)
        {
        set_addr(x,(py+y)); // start line
        for(px = 0; px < img->width   ; px += 8)
            {
            for(bi = 0; bi != 8; bi++)
                {
                setpixel((x + px + bi),(py+y),(*p & 1<<(7 - bi)));
                }
            p++;
            }
        p += padBytes;
        }


    }

void render(IMG *img,char x,char y)
    {
    int px,py;
    char *p,bi,padBytes;

    p = (char *)img->data;
//    left = 0;

    if (((img->width % 32) == 0) || ((img->width % 32) > 24))
        padBytes = 0;
    else if ((img->width % 32) <= 8)
        padBytes = 3;
    else if ((img->width % 32) <= 16)
        padBytes = 2;
    else
        padBytes = 1;


    for(py = img->height-1; py > -1; py--)
        {
        set_addr(x,(py+y)); // start line
        for(px = 0; px < img->width   ; px += 8)
            {
            for(bi = 0; bi != 8; bi++)
                {
                setpixel_m((x + px + bi),(py+y),(*p & 1<<(7 - bi)));
                }
            p++;
            }
        p += padBytes;
        }


    }

void render_or(IMG *img,char x,char y)
    {
    int px,py;
    char *p,bi,padBytes;

    p = (char *)img->data;
//    left = 0;

    if (((img->width % 32) == 0) || ((img->width % 32) > 24))
        padBytes = 0;
    else if ((img->width % 32) <= 8)
        padBytes = 3;
    else if ((img->width % 32) <= 16)
        padBytes = 2;
    else
        padBytes = 1;


    for(py = img->height-1; py > -1; py--)
        {
        set_addr(x,(py+y)); // start line
        for(px = 0; px < img->width   ; px += 8)
            {
            for(bi = 0; bi != 8; bi++)
                {
                if((*p & 1<<(7 - bi)))
                    setpixel_m((x + px + bi),(py+y),1);
                }
            p++;
            }
        p += padBytes;
        }


    }















void delay()
    {
    volatile int i=10; // breaks at around 5
    while(1)
        {
        i--;
        if(i ==0)
            break;
        }
    }

void fastwrite(char c)
    {
    unsigned short ports;

    ports =( iob_val & ~(WR | _CD)) << 8;
    ports |= c;


    if(i2cw16(i2cfd,GPIOA,ports) < 0)
        {
        perror("i2cw16 fastwrite");
        exit(1);
        }
    SET_L(CS);
    delay();
    SET_H(CS);
    }



void fastg(char *pf)
    {
    int i;

    set_addr(0,0);
    lcd_cmd(0xb0); //autowrite on
    for(i=0; i!= 1920; i++)
        {
        fastwrite(*pf++);
        }
    lcd_cmd(0xb2);

    }




// heating


void check_ovsw()
    {
    unsigned char bval;


    bval = i2cr8(i2cfd,GPIOB);

    if(bval & OVSW) // unconditionally switch of heating
        {
        ov = 1;
        heat = 0;
        iob_val |= OVST;
        iob_val &= ~HEAT;
        }
    else
        {
        ov = 0;
        iob_val &= ~ OVST;
        }
    if(i2cw8(i2cfd,GPIOB,iob_val) < 0)
        {
        perror("check_ovsw");
        exit(1);   // ignore err ?
        }
    }



void heat_on()
    {
    if(ov) //  ovverride to off = on
        return;

// else :-)

    printf("on\n");

    heat = 1;
    iob_val |= HEAT;
    if(i2cw8(i2cfd,GPIOB,iob_val) < 0)
        {
        perror("heat_on");
        exit(1);   // ignore err ?
        }
    }

void heat_off()
    {

    printf("off\n");
    heat = 0;
    iob_val &= ~HEAT;
    if(i2cw8(i2cfd,GPIOB,iob_val) < 0)
        {
        perror("heat_off");
        exit(1);   // ignore err ?
        }

    }

// geometry



void fill_rect(unsigned char x,unsigned char y,unsigned char w, unsigned char h,unsigned char set)
{
unsigned char xp,yp;

for(yp = y; yp != y+h; yp++)
    {
    for(xp = x; xp != x+w; xp++)
        {
        setpixel_m(xp,yp,set);
        }
    }


}






// Bresenham's circle

void plotCircle(int xm, int ym, int r)
    {
    int x = -r, y = 0, err = 2-2*r; /* II. Quadrant */
    do
        {
        setpixel_m(xm-x, ym+y,1); /*   I. Quadrant */
        setpixel_m(xm-y, ym-x,1); /*  II. Quadrant */
        setpixel_m(xm+x, ym-y,1); /* III. Quadrant */
        setpixel_m(xm+y, ym+x,1); /*  IV. Quadrant */
        r = err;
        if (r <= y)
            err += ++y*2+1;           /* e_xy+e_y < 0 */
        if (r > x || err > y)
            err += ++x*2+1; /* e_xy+e_x > 0 or no 2nd y-step */
        }
    while (x < 0);
    }



void plotArc(int xm, int ym, int r,int c)
    {
    int x = -r, y = 0, err = 2-2*r; /* II. Quadrant */
    do
        {
        switch(c)
            {
            case 4:
                setpixel_m(xm-x, ym+y,1); /*   I. Quadrant */
                break;
            case 3:
                setpixel_m(xm-y, ym-x,1); /*  II. Quadrant */
                break;
            case 1:
                setpixel_m(xm+x, ym-y,1); /* III. Quadrant */
                break;
            case 2:
                setpixel_m(xm+y, ym+x,1); /*  IV. Quadrant */
                break;
            }
        r = err;
        if (r <= y)
            err += ++y*2+1;           /* e_xy+e_y < 0 */
        if (r > x || err > y)
            err += ++x*2+1; /* e_xy+e_x > 0 or no 2nd y-step */
        }
    while (x < 0);
    }





//  line



void plotLine(int x0, int y0, int x1, int y1)
    {
    int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
    int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
    int err = dx+dy, e2; /* error value e_xy */

    for(;;)   /* loop */
        {
        setpixel_m(x0,y0,1);
        if (x0==x1 && y0==y1)
            break;
        e2 = 2*err;
        if (e2 >= dy)
            {
            err += dy;    /* e_xy+e_x > 0 */
            x0 += sx;
            }
        if (e2 <= dx)
            {
            err += dx;    /* e_xy+e_y < 0 */
            y0 += sy;
            }
        }
    }

void plotRectr(int xs,int ys, int w, int h,int r)
    {

///optimize horizontal lines !!
/// vertical ?
    int ye = ys + h;
    int xe = xs + w;


    plotLine(xs+r,ys,xe-r,ys);
    plotLine(xs+r,ye,xe-r,ye);

    plotLine(xs,ys+r,xs,ye-r);
    plotLine(xe,ys+r,xe,ye-r);

    plotArc(xs+r,ys+r,r,1);
    plotArc(xe-r,ys+r,r,2);
    plotArc(xs+r,ye-r,r,3);
    plotArc(xe-r,ye-r,r,4);


    }




void plotRectrs(int xs,int ys, int w, int h,int r)
    {

///optimize horizontal lines
/// vertical ?

    int ye = ys + h;
    int xe = xs + w;

    plotLine(xs+r,ys,xe-r,ys);
    plotLine(xs+r,ye,xe-r,ye);

    plotLine(xs,ys+r,xs,ye-r);
    plotLine(xe,ys+r,xe,ye-r);

    plotArc(xs+r,ys+r,r,1);
    plotArc(xe-r,ys+r,r,2);
    plotArc(xs+r,ye-r,r,3);
    plotArc(xe-r,ye-r,r,4);

    xs += 1;
    ys += 1;
    xe += 1;
    ye += 1;

//plotLine(xs+r,ys,xe-r,ys);
    plotLine(xs+r,ye,xe-r,ye);

//plotLine(xs,ys+r,xs,ye-r);
    plotLine(xe,ys+r,xe,ye-r);

//plotArc(xs+r,ys+r,r,1);
//plotArc(xe-r,ys+r,r,2);
//plotArc(xs+r,ye-r,r,3);
    plotArc(xe-r,ye-r,r,4);




    }
