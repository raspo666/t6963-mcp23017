#ifndef MCP23017_H
#define MCP23017_H

/* I'll use sequential addresses, for easy transfer of 16 bits */

#define IODIRA      0x00
#define IODIRB      0x01
#define IPOLA       0x02
#define IPOLB       0x03
#define GPINTENA    0x04
#define GPINTENB    0x05
#define DEFVALA     0x06
#define DEFVALB     0x07
#define INTCONA     0x08
#define INTCONB     0x09
#define IOCON       0x0a
#define IOCONINV    0x0b // INValid
#define GPPUA       0x0c
#define GPPUB       0x0d
#define INTFA       0x0e
#define INTFB       0x0f
#define INTCAPA     0x10
#define INTCAPB     0x11
#define GPIOA       0x12
#define GPIOB       0x13
#define OLATA       0x14
#define OLATB       0x15

/* IODIR */

#define INB(x,y)    (x |= (1<<y))
#define OUTB(x,y)   (x &= ~(1<<y))
#define INALL       0xff
#define OUTALL      0x0


/* put some logic to this WERK */
/* polarity */

#define IPOL        0
#define IPOLI       1

/* GPINTEN interrupt on change */

#define INTENABLE   1
#define INTDISABLE  0



/* IOCON */

#define BANK        0x80
#define MIRROR      0x40
#define SEQOP       0x20  // set = address does not increment
#define DISSLW      0x10  // slew rate on/off ??
#define HAEN        0x08  // needed if more that 1 device on bus
#define ODR         0x04  // set INT open drain
#define INTPOL      0x02  // if ODR==0 -> 1 = active H, 0 = active L


/* GPPU */

#define PULLUP      0xff // all pins





#define HEAT        0x80    // heater switch
#define OVSW        0x40    // override sw
#define OVST        0x20    // status LED







#endif // MCP23017_H
