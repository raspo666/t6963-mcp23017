/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * this file id part of https://github.com/rasp/t6963-mcp23017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */



#include "headers.h"


#define TPATH "/sys/bus/w1/devices/"
#define SENS  "28-81888d116461"   // serial ! change for your chip





void *read_temp(void *wurst)
    {
    char buffer[256],tval[32];
    int sens;

    while(1)
        {
        sens = open(TPATH SENS "/w1_slave", O_RDONLY);
        if(sens == -1)
            {
            perror("open sensor failed");
            pthread_exit(0);
            }


        if(read(sens, buffer, 256) > 0)
            {
            strncpy(tval,strstr(buffer,"t=")+2,5);
            pthread_mutex_lock(&m);
            temp = strtof(tval,NULL)/1000;
            pthread_mutex_unlock(&m);
            }
        else
            pthread_exit(0);
        close(sens);
        sleep(1);
        }
    }
