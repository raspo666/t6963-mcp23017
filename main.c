/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * this file id part of https://github.com/rasp/t6963-mcp23017
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include "headers.h"


int i2cfd;

typedef struct infoline{
    char line[32];
    struct infoline *next;
}INFOLINE;


INFOLINE *infostart;
char infocl[] ="                                        ";

float temp;
char *pf;  // playfield "framebuffer"

pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

Weather *weather;

IMG *clear,*cloud,*rain,*snow;

void load_ubahn()
    {
    FILE *bahn;
    char *p,lbuf[32];
    INFOLINE *ip;
    int i =0;

    ip = infostart;

    if(!(bahn = fopen("/var/u-bahn","rb")))
        exit(printf("open u-bahn fail \n"));
    if(fgets((char *)&ip->line,32,bahn) == NULL)
        exit(printf("empty u-bahn file \n"));
    ip->next = NULL;

    while(1)
        {
//        if(strlen(ip->line) < 5)
//            continue;

        printf("read line %d -> %s\n",i++,ip->line);
        if(fgets(lbuf,32,bahn))
            {
            if(ip->next == NULL)
                {
                ip->next = malloc(sizeof(INFOLINE));
                if(ip->next == NULL)
                    exit(printf("infoline malloc next\n"));
                ip->next->next = NULL;
                }
            ip = ip->next;
            strcpy((char *)&ip->line,lbuf);
            }
        else
            {
            break;
            }
        }
    ip = NULL; //->next = NULL;
    i = 0;
    ip = infostart;
    while(ip)
        {
        printf("ip line %d ->%s<-\n",i++,ip->line);
        ip = ip->next;
        }
    fclose(bahn);

    }

void loadw()
    {

    clear = load_bmp("img/clear.bmp");
    cloud = load_bmp("img/cloud.bmp");
    rain = load_bmp("img/rain.bmp");
    snow = load_bmp("img/snow.bmp");

    weather = malloc(sizeof(Weather));
    if(weather == NULL)
        {
        perror("no mem for weather");
        exit(1);
        }

    }

void read_weather()
    {
    int code;
    float tempk;
    FILE *wfile;
    if(!(wfile = fopen("/var/wvals","rb")))
        {
        perror("wfile open");
        return;
        }
    fscanf(wfile,"%d",&code);

    switch(code)
        {
        case 300 ... 321:
        case 500 ... 531:
            weather->ico = rain;
            break;
        case 600 ... 622:
            weather->ico = snow;
            break;
        case 800:
            weather->ico = clear;
            break;
        case 801 ... 804:
            weather->ico = cloud;
            break;
        }


    fscanf(wfile,"%f",&tempk);
    weather->temp = tempk - 273.15;

    fclose(wfile);
    }



int main()
    {
    int i,firstrun = 1,dir = 0;
    char buffer[256];
    IMG *img;
    FNT *small,*medium,*large;
    time_t now,then;
    struct tm *local;
    struct timeval t;
    char *day[7]=
        {
        "Sun","Mon","Tue","Wed","Thu","Fri","Sat"
        };

    INFOLINE *li;

    pthread_t tid1;


    pthread_create(&tid1, NULL, read_temp, NULL);


    loadw();
    ginit();

    pf = malloc(30*64);
    memset(pf,0xff,30*64);

    infostart = malloc(sizeof(INFOLINE));
    if(infostart == NULL)
        exit(printf("no mem for infostart\n"));
    infostart->next = NULL;
    li = infostart;

    lcd_init();
    clr();


    set_gr();
    clr_gr();

    small = loadfont("img/smaller/ser",32,255);
    medium = loadfont("img/small/uni",32,255);
//    large = loadfont("img//large/uni",32,255);


    default_font = medium;


    img = load_bmp("logo.bmp");
    render(img,20,0);
    fastg(pf);
//    show_image(img,20,0);
//  getchar();
    sleep(4); // showoff :-)

    clr_gr();

    while(1)
        {
        if(pthread_kill(tid1,0) != 0)
            {
            perror("");
            printf("temp sensor thread has left the building\n");
            }
        pthread_mutex_lock(&m);
        sprintf(buffer,"%.2fC",temp);
        pthread_mutex_unlock(&m);
        text_at_f(buffer,4,19);
        plotRectrs(0,19,59,16,4);   // always redraaw, as text overwrites some pixels

        check_ovsw();
        if(ov)
            text_at_f("Manual",66,19);
        else
            text_at_f(" Auto ",66,19);
        plotRectrs(64,19,59,16,4);

        if(heat == 1 && ov == 0)
            text_at_f(" Heizt",126,19);
        else
            text_at_f("  Aus ",126,19);
        plotRectrs(127,19,59,16,4);



        if(temp < 20.0 && ov == 0 && heat == 0)
            heat_on();
        else if(temp > 20.8 && heat == 1)
            heat_off();

        //img = load_bmp("img/rain.bmp");
        if(now % 60 == 0 || firstrun == 1) //every minute
            {
            read_weather();
            sprintf(buffer,"%0.0fC",weather->temp);
            render(weather->ico,200,4);
            text_at_f("    ",198,19);
            text_at_f(buffer,198,19);
            render_or(weather->ico,200,4);
            plotRectrs(190,1,48,34,4);
            load_ubahn();
            li = infostart;
            }
        if(now % 10 == 0 || firstrun == 1)
            {
            fill_rect(1,39,236,22,0);
            default_font = small;
            text_at_f(li->line,20,39);
            printf("line 1 ->%s<-\n",li->line);
            li = li->next;

            printf("line 2 ->%s<-\n",li->line);
            text_at_f(li->line,20,49);
            li = li->next;
            if(li == NULL)
                li = infostart;
            plotRectrs(0,38,237,23,4);
            default_font = medium;
            }
        then = time(0);
        while((now = time(0)) == then);



        local = localtime(&now);
        sprintf(buffer,"%s %02d/%02d  %02d:%02d:%02d",day[local->tm_wday],local->tm_mday,local->tm_mon+1,local->tm_hour,local->tm_min,local->tm_sec);
        text_at_f(buffer,5,1);
        plotRectrs(0,0,(strlen(buffer) * default_font->glyp[32]->width)+14,16,4);

        fastg(pf);
        firstrun = 0;
        }

    }
