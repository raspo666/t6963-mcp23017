/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * this file id part of https://github.com/rasp/t6963-mcp23017
*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */




#include "headers.h"


int i2c_open(char *name)
    {
    int fd;


    fd = open(name,O_RDWR);

    if(fd < 0)
        {
        perror("i2c_open");
        exit(1);
        }

    return fd;
    }



void enslave(int address,int fh)
    {

    if (ioctl(fh, I2C_SLAVE, address) < 0)
        {
        perror("enslave failed");
        exit(1);
        }
    }




