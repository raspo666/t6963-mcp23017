/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * this file id part of https://github.com/rasp/t6963-mcp23017
*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */



#ifndef I2C_H
#define I2C_H

#include "headers.h"


/*
* set speed:
* fdtput --type u  /boot/bcm2835-rpi-b-rev2.dtb /soc/i2c@7e804000 clock-frequency 1700000
*/



#define I2CDEV "/dev/i2c-1"  /// ugly,WERK !

#define MCP 0x27

extern int i2cfd;

int i2c_open(char *name);
#define i2_close(x) close(x);

void enslave(int address,int fh);

#define i2cr8(f,r)      i2c_smbus_read_byte_data(f, r)
#define i2cr16(f,r)     i2c_smbus_read_word_data(f, r)

#define i2cw8(f,r,v)      i2c_smbus_write_byte_data(f, r, v)
#define i2cw16(f,r,v)    i2c_smbus_write_word_data(f, r, v)







#endif // I2C_H
